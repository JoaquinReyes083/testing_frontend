context('Actions', () => {
    beforeEach(() => {
        cy.visit('https://gitlab.com/users/sign_in#register-pane')
    })

    it('.type()', () => {
        cy.get('.qa-new-user-email')
        .type('kinreyes12@gmail.com').should('have.value', 'kinreyes12@gmail.com')
        
        cy.get('.qa-new-user-email-confirmation')
        .type('kinreyes12@gmail.com').should('have.value', 'kinreyes12@gmail.com')
        
        cy.get('.qa-new-user-name')
        .type('Joaquito').should('have.value', 'Joaquito')

        cy.get('.qa-new-user-username')
        .type('Joaco_343432324').should('have.value', 'Joaco_343432324')

        cy.get('.qa-new-user-password')
        .type('blablabla').should('have.value', 'blablabla')

        cy.get('.qa-new-user-accept-terms')
        .check();

        cy.get('#new_user_email_opted_in')
        .check();

        cy.get('.qa-new-user-register-button')
        .click();
    });
})   